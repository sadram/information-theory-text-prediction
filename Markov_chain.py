import math
import numpy as np


def bookiidentropy(book):
    """ Compute entropy of a chosen
    book with iid model
    :return: book iid entropy
    :param book: given book"""
    try:
        isinstance(book, basestring)
    except:
        print ("Argument to bookiidentropy is not a string-type.")
    f = open(book, 'r')
    string = f.read().decode('utf8').lower()
    prob = [float(string.count(c)) / len(string) for c in dict.fromkeys(list(string))]

    # compute the entropy
    entropy = - sum([p * math.log(p) / math.log(2.0) for p in prob])

    return entropy


def crossent(book_1, book_2):
    """ Compute cross entropy of two books
    for iid model
    :param book_2, book_1: given books
    :return cross_ent : cross entropy between the two books
    with book_1 as reference
    """
    # Open the two books
    f1 = open(book_1, 'r')
    f2 = open(book_2, 'r')

    # Read them and encore to utf8 format
    string1 = f1.read().decode('utf-8').lower()
    string2 = f2.read().decode('utf-8').lower()

    # Compute intersection of the two books better solution would be to add small proba to symbol
    # Can also be like (1-eps)P(model trin on A) + eps*P(uniform over all symbols)
    inter = set(dict.fromkeys(list(string1))).intersection(dict.fromkeys(list(string2)))

    # Compute probability
    p = [float(string1.count(c)) / len(string1) for c in inter]
    q = [float(string2.count(c)) / len(string2) for c in inter]

    # Calculate the cross-entropy
    cross_ent = - sum([p[i] * math.log(q[i]) / math.log(2.0) for i in range(len(inter))])

    return cross_ent


def kl(book1, book2):
    """
    Kullback Liebler divergence between two books
    :param book1:
    :param book2:
    :return : Kullback Libeler divergence
    """
    return crossent(book1, book2) - bookiidentropy(book1)


def ent_markov(book, k):
    """ Compute the entropy of a book based on Markov
    model of order k
    :param k:
    :param book: """
    try:
        isinstance(book, basestring)
    except:
        print ("Argument to bookiidentropy is not a string-type.")
    f = open(book, 'r', encoding='utf-8')
    string = f.read().lower()
    dic = {}
    for i in range(len(string) - k - 1):
        if string[i:i + k] not in dic.keys():
            dic[string[i:i + k]] = dict.fromkeys(list(string), 0)
            dic[string[i:i + k]][string[i + k]] = 1
        else:
            dic[string[i:i + k]][string[i + k]] += 1
    # For each value of X compute related entropy
    h = [0] * len(list(dic.keys()))
    count = 0
    for i in dic.keys():
        # Number of total occurences
        num = sum(dic[i].values())
        for j in dic[i].keys():
            if dic[i][j] > 0:
                proba = float(dic[i][j]) / float(num)
                h[count] -= proba * math.log(proba) / math.log(2.0)
                dic[i][j] = proba
        count += 1
    prob = [float(string.count(c)) / (len(string) - k) for c in dic.keys()]
    # calculate the entropy
    entropy = sum([prob[i] * h[i] for i in range(len(h))])

    return {'ent': entropy, 'dic': dic, 'prob': prob}


def generate(dic):
    """
    generate sentence from proba distribution
    :param dic:
    :return string: given sentence
    """
    # Create list of possible first characters
    characters = list(dic.keys())
    # Order of the markov chain
    k = len(characters[1])
    # Choose first characters at random
    string = characters[np.random.randint(1, len(characters), 1)]

    # First choose a character
    chardic = list(dic[string].keys())
    chosen = np.random.choice(chardic, 1, p=dic[string].values())
    while chosen != u'.':
        string = string + chosen[0]
        chosen = np.random.choice(chardic, 1, p=dic[string[-k:]].values())
    return string


def cross_ent_markov(prob, dic1, dic2):
    """
    Cross entropy for Markov model
    :param prob:
    :param dic1, dic2: Proba dictionaries of two books
    :return cross_entropy: cross_entropy
    """
    h = [0] * len(list(dic1.keys()))
    count = 0
    for i in dic1.keys():
        if i in dic2.keys():
            for j in dic1[i].keys():
                if j in dic2[i].keys() and dic1[i][j] > 0 and dic2[i][j] > 0:
                    p = dic1[i][j]
                    q = dic2[i][j]
                    h[count] -= p * math.log(q) / math.log(2.0)
        count += 1
    cross_entropy = sum([prob[i] * h[i] for i in range(len(h))])

    return cross_entropy


def test():
    print(bookiidentropy('texts/Goethe.txt'))
    print(crossent('texts/Goethe.txt', 'texts/Hamlet.txt'))
    print(kl('texts/Goethe.txt', 'texts/Hamlet.txt'))
    res = ent_markov('texts/Goethe.txt', 3)
    res2 = ent_markov('texts/Hamlet.txt', 3)
    print(res['ent'])
    print(cross_ent_markov(res['prob'], res['dic'], res2['dic']))
    print(generate(res['dic']))


if __name__ == "__main__": test()
