#!/usr/bin/env python3
# encoding: utf-8

from __future__ import division

from collections import defaultdict
from os import listdir
from os.path import isfile, join


import string
# import pprint as pp
import sys

"""AML Lab

Usage: python3 main.py <path>
"""


def select_text(my_path, presel=None):
    """Select a text file in a given path

    Args:
        my_path: The path to the directory containing the text files.

        presel: The optional number of the preselected file
        (must be in the range [0, #files])

    Returns:
        A hash table containing the character frequency
    """
    file_list = [f for f in listdir(my_path) if isfile(join(my_path, f))]
    if presel is None:
        for i in range(file_list.__len__()):
            print(i, ": ", file_list[i])
        presel = input("Select file ")
    try:
        selection = file_list[int(presel)]
    except IndexError:
        print("%s is not a valid choice", presel)

    return join(path, selection)


def analyse_chars(file):
    """Counts the number of different letters in a file

    Args:
        file: A UTF-8 text document.

    Returns:
        A hash table containing the character frequency
    """
    alphabet = [x for x in string.ascii_lowercase]
    alphabet_frequency = dict.fromkeys(alphabet, 0)

    with open(file, mode='r', encoding='utf-8') as f:
        for lines in f:
            line = lines.lower().strip()
            for chars in line:
                char = chars.lower()
                if char.isalnum() and char in alphabet:
                    alphabet_frequency[char.lower()] += 1
    sum = 0
    for v in alphabet_frequency.values():
        sum += v

    initstats = lambda: defaultdict(initstats)
    stats = initstats()
    for k,v in alphabet_frequency.items():
        stats[k]['count'] = v
        stats[k]['proba'] = alphabet_frequency[k] / sum

    return stats


def print_stats(stats):
    psum = 0
    for k in sorted(stats.keys()):
        psum += stats[k]['proba']
        print("letter: {}, count = {}, probability = {}".format(k, stats[k]['count'], stats[k]['proba']))
    print("total probability :", psum)



def main(text_dir):
    """Print the letter usage statistics of a selected file.

    Args:
        text_dir: path to the text files
    """
    text = select_text(text_dir, 1)
    stats = analyse_chars(text)
    print_stats(stats)


if __name__ == '__main__':

    # free shakespear texts from Gutenberg
    path = r'./texts/'

    main(path)


